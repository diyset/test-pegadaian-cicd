package com.docker.test.pegadaian.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/")
public class IndexController {

    @GetMapping
    public Map<String,String> index(){
        HashMap<String,String> hasil = new HashMap<>();
        hasil.put("codename","Prodcution");
        return hasil;
    }



}
